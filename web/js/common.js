var ajaxUrl = (function () {
	var a = (document.URL).split('/');
	return "http://"+a[2]+"/";
})();

/**
 * Voer een ajax request uit (alle waardes worden altijd gepost
 * via de url kan je get waardes sturen
 * @param String url
 * @param Object data
 * @param String resultTarget (id van het element waar het resultaat in terecht moet komen)
 * @returns void;
 */
function performAction(url,data,resultTarget)
{
	//Add formdata
	formdata = {};

	for(s in data){formdata[s] = data[s];}

	//perform request
	$.ajax({
			type: "POST",
			url: ajaxUrl+url,
			data: formdata,
			dataType: "html",
		  	success: function(data) {
		  			$(resultTarget).empty();
		  			$(resultTarget).html(data);
		  	}
	});
}


/**
 * Parse een formulier naar een object met behulp van jquery serialize 
 * @param formid
 * @returns Object met formulier waardes
 */
function parseFormToObject(formid)
{
	var form 	= $(formid).serialize();
	var a		= form.split('&');
	var o		= new Object();
	
	for(var i=0; i < a.length; i++)
	{
		if(a[i].trim() !== '')
		{
			var kv = a[i].trim().split('=');
			
			if(kv.length === 2)
			{
				if(kv[0].trim() !== '')
				{
					var key = kv[0];
					var value = unescape(kv[1]); //<-Jquery vernaggelt de waardes voor het posten
					o[key] = value;
				}
			}
		}
	}
	return o;
}

/**
 * Toggle een class op een element
 * @param {type} id
 * @param {type} cssClass
 * @returns {undefined}
 */
function toggleClass(id,cssClass)
{
	if($(id).length > 0)
	{
		$(id).each(function(){
			if($(this).hasClass(cssClass))
			{
				$(this).removeClass(cssClass);
			}else
			{
				$(this).addClass(cssClass);
			}
		});
	}
}

/**
 * Validate e-mail address 
 * (Niet zelf geschreven)
 * @param email
 * @returns Boolean
 */
 function validateEmail(email)
 {
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	return emailReg.test(email);
}

/**
 * Implement trim functions
 * (Niet zelf geschreven, wel prototype constructie gemaakt (actionscript 1.0 dingetje)
 */
if(!String.prototype.trim)
{
	String.prototype.trim		=	function(){return this.replace(/^\s+|\s+$/g, '');};
	String.prototype.ltrim		=	function(){return this.replace(/^\s+/,'');};
	String.prototype.rtrim		=	function(){return this.replace(/\s+$/,'');};
	String.prototype.fulltrim	=	function(){return this.replace(/(?:(?:^|\n)\s+|\s+(?:$|\n))/g,'').replace(/\s+/g,' ');};
}

















/**Haal het login formulier op */
function menuLogin()
{
	performAction('login',{},'#main_content');
}

/**Ga naar de register pagina */
function menuRegister()
{
	performAction('register',{},'#main_content');
}

/**
 * Haal een specifieke post op om te editten
 * @param id int
 * @returns void
 */
function postEditGet(id)
{
	performAction('post_edit/'+id,{},'#post_create_form');
}

function postUpdateInsert()
{
	console.log('postUpdateInsert');
	var validated = true;

	if($('#post_create_form .error').length > 0) $('#post_create_form .error').removeClass('error');
	
	if($('#title').val().trim() === '')
	{
		$('#title_label').addClass('error');
		$('#title').addClass('error');
		validated = false;
	}
		
	if($('#message').val().trim() === '')
	{
		$('#message_label').addClass('error');
		$('#message').addClass('error');
		validated = false;
	}
	
	if(validated)
	{
		var o = parseFormToObject('#form_post');
		performAction('post_update_insert',o,'#main_content');
	}
}

function postCancel()
{
	$('#post_create_form').empty();
}

function postReadFull(id)
{
	toggleClass('#post_'+id+'_short','hidden');
	toggleClass('#post_'+id+'_full','hidden');
}

function postDelete(id)
{
	performAction('post_delete/'+id, {} ,'#main_content');
}





/**
 * Registreer een nieuwe author
 * @returns void
 */
function authorRegister()
{
	var validated = true;

	if($('#author_register_form .error').length > 0) $('#author_register_form .error').removeClass('error');
	
	//Simpele vaidatie
	$('#author_register_form input[type=text]').each(function(){
		if($(this).val() == "")
		{
			validated = false;
			$(this).addClass('error');
		}
	});
	
	//Check e-mail address
	if(!validateEmail($('#email').val()))
	{
		validated = false;
		$('#email').addClass('error');
	}
	
	//Check Wachtwoorden (of ze hetzelfde zijn)
	if($('#password').val().trim() !==  $('#repeat_password').val().trim())
	{
		validated = false;
		$('#password').addClass('error');
		$('#repeat_password').addClass('error');
	}
	
	
	if(validated)
	{
		var o = parseFormToObject('#author_register_form');
		performAction('author_register',o,'#main_content');
	}
}
