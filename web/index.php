<?php

use Silex\Application;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\TwigServiceProvider;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;

mb_internal_encoding("UTF-8");
mb_http_output( "UTF-8" );

/**Application autoloader*/
$loader = require_once __DIR__.'/../vendor/autoload.php';

/**Laad eigen classpath */
$loader->set('Openblog', __DIR__ . '/../');

/**Maak de applicatie aan */
$app = new Application();

/**De template engine initialiseren TWIG **/
$app->register(new TwigServiceProvider(), array(
	'twig.path' => __DIR__.'/../views',
));

/**De doctrine Service Pdo variant**/
$app->register(new DoctrineServiceProvider(), array(
    'db.options' => array(
        'driver'   => 'pdo_mysql',
		'dbname' => 'openblog',
		'host' => 'localhost',
		'user' => 'root',
		'password' => '',
		'charset' => 'utf8',
    ),
));

/**Sessie provider (hiermee bouwen we een login)**/
$app->register(new SessionServiceProvider());

$app['session.db_options'] = array(
	'db_table' => 'session',
	'db_id_col' => 'id',
	'db_data_col' => 'value',
	'db_time_col' => 'time',
);

$app['session.storage.handler'] = $app->share(function () use ($app) {
	return new PdoSessionHandler(
		$app['db']->getWrappedConnection(),
		$app['session.db_options'],
		$app['session.storage.options']
	);
});


$app['session']->start();

//Handle Index page
$app->match('/', 'Openblog\Controller\IndexController::handleIndexRequest');


//--------------------------------------------------------------------------------------
//Register formulier ophalen
$app->match('/register', 'Openblog\Controller\IndexController::menuRegister');

//Registreer een author
$app->match('/author_register', 'Openblog\Controller\AuthorController::authorRegister');

//--------------------------------------------------------------------------------------
//Login formulier ophalen
$app->match('/login', 'Openblog\Controller\IndexController::menuLogin');

//Login
$app->match('/author_login', 'Openblog\Controller\AuthorController::authorLogin');

//--------------------------------------------------------------------------------------
//Logout 
$app->match('/author_logout', 'Openblog\Controller\IndexController::authorLogout');

//--------------------------------------------------------------------------------------
//Haal alle blogposts van een author op
$app->match('/author/{id}', 'Openblog\Controller\AuthorController::authorOverview');

//--------------------------------------------------------------------------------------
//Haal een post op om te bewerken
$app->match('post_edit/{id}', 'Openblog\Controller\PostController::postEdit');

//Sla een post op na bewerking
$app->match('post_update_insert', 'Openblog\Controller\PostController::postUpdateInsert');

//Verwijder een post
$app->match('post_delete/{id}', 'Openblog\Controller\PostController::postDelete');

//Lees een post
$app->match('post/{id}', 'Openblog\Controller\PostController::readPost');

$app['debug'] = true; 
$app->run();
