<?php

namespace Openblog\Model;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;

class QuoteModel extends BaseModel
{

    public function __construct(Request $request, Application $app)
    {
        $allowed = array('quote', 'author');

        $type           = array();
        $type['quote']  = \PDO::PARAM_STR;
        $type['author'] = \PDO::PARAM_STR;

        parent::__construct($request, $app, 'quote', $allowed, $type);
    }

    /**
     * Haal een enkele quote op uit de database
     * @return array
     */
    public function getRandomQuote()
    {
        $quote = $this->conn->prepare("SELECT * FROM `".$this->table."` ORDER BY RAND() LIMIT 1");
        $quote->execute();
        return $quote->fetch();
    }
}