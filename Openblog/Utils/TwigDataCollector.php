<?php

namespace Openblog\Utils;

class TwigDataCollector
{

	/**
	 * @var \Openblog\Utils\TwigDataCollector
	 */
	private static $INST = null;

	/**
	 * Data
	 * @var array 
	 */
	private $data = null;

	private function __construct() {
		$this->data = array();
	}

	/**
	 * @return \Openblog\Utils\TwigDataCollector
	 */
	public static function getInstance() {
		if (self::$INST === null) {
			self::$INST = new TwigDataCollector();
		}

		return self::$INST;
	}

	/**
	 * Voeg een waarde toe
	 * @param atring $key
	 * @param arry/string $value
	 */
	public function set($key, $value) {
		$this->data[$key] = $value;
	}

	/**
	 * Verwijder een waarde
	 * @param string $key
	 */
	public function remove($key) {
		if (array_key_exists($key, $this->data))
			unset($this->data, $key);
	}

	/**
	 * Returns all data
	 * @return type
	 */
	public function getAll() {
		return $this->data;
	}

}
