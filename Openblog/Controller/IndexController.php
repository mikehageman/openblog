<?php

namespace Openblog\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Openblog\Model\AuthorModel;
use Openblog\Model\PostModel;
use Openblog\Model\QuoteModel;
use Openblog\Utils\TwigDataCollector;

Class IndexController extends BaseController {

	public function __construct()
	{
		
	}

	/**
	 * Handel de index request af...
	 * @param Symfony\Component\HttpFoundation\Request $request
	 * @param Silex\Application $app
	 * @return type
	 */
	public function handleIndexRequest(Request $request, Application $app)
	{
		TwigDataCollector::getInstance()->set('posts', (new PostModel($request, $app))->getAll());
		TwigDataCollector::getInstance()->set('authors', (new AuthorModel($request, $app))->getAll());
		TwigDataCollector::getInstance()->set('quote', (new QuoteModel($request, $app))->getRandomQuote());

		$this->getAuthorLoginInformation($app);

		return $app['twig']->render('quote/blog.quote.twig', TwigDataCollector::getInstance()->getAll());
	}

	/**
	 * Uitloggen als auteur
	 * @param Request $request
	 * @param Application $app
	 * @return type
	 */
	public function authorLogout(Request $request, Application $app)
	{
		$app['session']->clear();
		return $app->redirect('/');
	}

	/**
	 * Render het registratie formulier
	 * @param Symfony\Component\HttpFoundation\Request $request
	 * @param Silex\Application $app
	 * @return type
	 */
	public function menuRegister(Request $request, Application $app)
	{
		return $app['twig']->render('author/blog.new.author.twig');
	}

	/**
	 * Render het login formulier
	 * @param Symfony\Component\HttpFoundation\Request $request
	 * @param Silex\Application $app
	 * @return type
	 */
	public function menuLogin(Request $request, Application $app)
	{
		TwigDataCollector::getInstance()->set('authors', (new AuthorModel($request, $app))->getAll());
		return $app['twig']->render('login/blog.login.twig', TwigDataCollector::getInstance()->getAll());
	}

}
